import { Component, OnInit } from '@angular/core';
import { MessageService } from '../message.service';
import { DistroService } from "../distro.service";
import { Distro } from "../distro";

@Component({
  selector: 'app-distros',
  templateUrl: './distros.component.html',
  styleUrls: ['./distros.component.css']
})
export class DistrosComponent implements OnInit {
  distros: Distro[];

  selectedDistro: Distro;
  
  constructor(private distroService: DistroService, private messageService: MessageService) { }
  
  ngOnInit(): void {
    this.getDistros();
  }

  getDistros(): void{
    this.distroService.getDistros()
      .subscribe(distros => this.distros = distros);
  }

  add(name: string): void {
    name = name.trim();
    if (!name) { return; }
    this.distroService.addDistro({ name } as Distro)
      .subscribe(distro => this.distros.push(distro));
  }

  delete(distro: Distro): void{
    this.distros = this.distros.filter(d => d !== distro);
    this.distroService.deleteDistro(distro).subscribe();
  }
}
