import {Distro} from "./distro";

export const DISTROS: Distro[] = [
	{id: 1 , name: 'Arch Linux'},
	{id: 2 , name: 'Debian'},
	{id: 3 , name: 'Linux Mint' },
	{id: 4 , name: 'Kali Linux'},
	{id: 5 , name: 'Fedora'},
	{id: 6 , name: 'Gentoo'},
	{id: 7 , name: 'Manjaro'},
	{id: 8 , name: 'Ubuntu'},
	{id: 9 , name: 'Parriot'},
	{id: 10 , name: 'Puppy'},
]