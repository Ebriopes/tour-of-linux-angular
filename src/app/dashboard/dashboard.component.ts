import { Component, OnInit } from '@angular/core';
import { DistroService } from "../distro.service";
import { Distro } from "../distro";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  distros: Distro[];

  constructor(private distroService: DistroService) { }

  ngOnInit(): void {
    this.getDistros();
  }

  getDistros(): void{
    this.distroService.getDistros()
      .subscribe(distros => this.distros = distros.slice(0, 4));
  }
}
