import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DistroSearchComponent } from './distro-search.component';

describe('DistroSearchComponent', () => {
  let component: DistroSearchComponent;
  let fixture: ComponentFixture<DistroSearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DistroSearchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DistroSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
