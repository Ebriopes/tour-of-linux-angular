import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';

import {
  debounceTime, distinctUntilChanged, switchMap
} from 'rxjs/operators';

import { Distro } from "../distro";
import { DistroService } from "../distro.service";

@Component({
  selector: 'app-distro-search',
  templateUrl: './distro-search.component.html',
  styleUrls: ['./distro-search.component.css']
})
export class DistroSearchComponent implements OnInit {
  distros$: Observable<Distro[]>
  private searchTerms = new Subject<string>();

  constructor(private distroService: DistroService) { }
  
  search(term: string): void{
    this.searchTerms.next(term);
  }  
  
  ngOnInit(): void {
    this.distros$ = this.searchTerms.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap((term: string)=> this.distroService.searchDistros(term))
    )
  }
}
      