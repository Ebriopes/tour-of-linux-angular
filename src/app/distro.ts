export interface Distro {
	id: number;
	name: string;
}