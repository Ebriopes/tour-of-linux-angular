import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { catchError, map, tap } from 'rxjs/operators';
import { Observable, of} from "rxjs";
import { MessageService } from "./message.service";
import { Distro } from "./distro";
//import { DISTROS } from "./mock_distros";

@Injectable({
  providedIn: 'root'
})
export class DistroService {
  private distroUrl = 'api/distros';
  
  HttpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }

  constructor(
    private http: HttpClient,
    private messageService: MessageService
  ) { }
    
  getDistro(id: number): Observable<Distro> {
    const url = `${this.distroUrl}/${id}`
    return this.http.get<Distro>(url)
      .pipe(
        tap(_ => this.log(`Fetched distro id=${id}`)),
        catchError(this.handleError<Distro>(`getDistro id=${id}`))
        )
  }
  
  getDistros(): Observable<Distro[]> {
    return this.http.get<Distro[]>(this.distroUrl)
    .pipe(
      tap(_ => this.log('Fetched distros')),
      catchError(this.handleError<Distro[]>('getDistros', []))
      );
  }
  
  updateDistro(distro: Distro): Observable<Distro> {
    return this.http.put(this.distroUrl, distro, this.HttpOptions)
    .pipe(
      tap(_ => this.log(`Updated distro id=${distro.id}`)),
      catchError(this.handleError<any>('updateDistro'))
      );
  }
  
  addDistro(distro: Distro): Observable<Distro> {
    return this.http.post<Distro>(this.distroUrl, distro, this.HttpOptions)
      .pipe(
        tap((newDistro: Distro) => this.log(`Distro ${newDistro.name} added`)),
        catchError(this.handleError<Distro>('addDistro'))
      );
  }
    
  deleteDistro(distro: Distro | number): Observable<Distro> {
    const id = typeof distro === 'number' ? distro : distro.id;
    const url = `${this.distroUrl}/${id}`;

    return this.http.delete<Distro>(url, this.HttpOptions).pipe(
      tap(_ => this.log(`Deleted distro id=${id}`)),
      catchError(this.handleError<Distro>('deleteDistro'))
    );
  }

  searchDistros(term: string): Observable<Distro[]>{
    if (!term.trim()) { return of([]) }
    return this.http.get<Distro[]>(`${this.distroUrl}/?name=${term}`).pipe(
      tap(x => x.length ?
        this.log(`found distros matching "${term}"`) :
        this.log(`no distros matching "${term}"`)),
      catchError(this.handleError<Distro[]>('searchDistros', []))
    )
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      this.log(`${operation} failed: ${error.message}`);
      return of((result as T));
    }
  }
  
  private log(message: string) {
    this.messageService.add(`DistroService: ${message}`)
  }
}