import { Injectable } from '@angular/core';
import { InMemoryDbService } from "angular-in-memory-web-api";
import { Distro } from './distro';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const distros = [
      {id: 11 , name: 'Arch Linux'},
      {id: 12 , name: 'Debian'},
      {id: 13 , name: 'Linux Mint' },
      {id: 14 , name: 'Kali Linux'},
      {id: 15 , name: 'Fedora'},
      {id: 16 , name: 'Gentoo'},
      {id: 17 , name: 'Manjaro'},
      {id: 18 , name: 'Ubuntu'},
      {id: 19 , name: 'Parriot'},
      {id: 20 , name: 'Puppy'},
    ]
    return { distros }
  }

  constructor() { }

  genId( distros: Distro[]): number {
    return distros.length > 0 ? Math.max(...distros.map(hero=>hero.id)) +1 : 1
  }
}
