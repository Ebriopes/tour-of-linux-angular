import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Distro } from "../distro";
import { DistroService } from '../distro.service';

@Component({
  selector: 'app-distro-detail',
  templateUrl: './distro-detail.component.html',
  styleUrls: ['./distro-detail.component.css']
})
export class DistroDetailComponent implements OnInit {
  distro: Distro;
  
  constructor(
    private route: ActivatedRoute,
    private distroService: DistroService,
    private location: Location
  ) { }

  ngOnInit(): void {
    this.getDistro();
  }

  getDistro(): void{
    const id = +this.route.snapshot.paramMap.get('id');
    this.distroService.getDistro(id)
      .subscribe(distro => this.distro = distro);
  }

  goBack(): void{
    this.location.back();
  }
  save(): void{
    this.distroService.updateDistro(this.distro)
      .subscribe(() => this.goBack());
  }
}
