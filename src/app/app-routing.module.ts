import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DistrosComponent } from "./distros/distros.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { DistroDetailComponent } from './distro-detail/distro-detail.component';

const routes: Routes = [
  { path: 'distros', component: DistrosComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'detail/:id', component: DistroDetailComponent },
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { };